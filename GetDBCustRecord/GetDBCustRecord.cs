using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using CosmoDB.Console.Model;
using Microsoft.Azure.Cosmos;
using System.Linq;
using System.Collections.Generic;

namespace GetDBCustRecord
{
    public static class GetDBCustRecord
    {
        [FunctionName("GetDBCustRecord")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function processed a request.");
            
            string Id = req.Query["Id"];

            string name = req.Query["name"];

            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            dynamic data = JsonConvert.DeserializeObject(requestBody);
            Id = Id ?? data?.Id;
            name = name ?? data?.name;
            var cust = new Customer();

            string responseMessage = "";
            //if (!string.IsNullOrEmpty(Id))
            //{


            //}
            //else
            //{
                 var customer = new Customer();

        //using (var client = new CosmosClient("AccountEndpoint=https://mindcept-cosmou.documents.azure.com:443/;AccountKey=GQPYnevJog3Llbqt3okijN3qg53Vpg8SKu4Azz41YmgLeS8z3mchYfxFl77KjUfawcKaH2MBMug3v1GtuIRh3g==;"))
        //{

        //    using (var client = new CosmosClient("AccountEndpoint=https://mindcept-cosmo.documents.azure.com:443/;AccountKey=QQ8vUzwQtI4H3DQG6OYvuVUDaswQuNcoZBgtP9qnzjnR97UVHg0AIV5eKELIPFnWszn9Zuqr7XiuffQep6rjHg==;"))
        //    {
        //   //     var DB = client.GetDatabase("ChampionProductCosmoDB");
        //    //    var table = client.GetContainer(DB.Id, "Customer2");
        //        var DB = client.GetDatabase("ChampionProductsCosmoDB");
        //    var table = client.GetContainer(DB.Id, "Customer2");

        //    //loop through all records
        //    var sqlString = "Select * from c where c.customerId = " + Id;
        //    //var sqlString = "Select * from customer";
        //    var query = new QueryDefinition(sqlString);
        //    FeedIterator<Customer> iterator = table.GetItemQueryIterator<Customer>(sqlString);

        //    FeedResponse<Customer> queryResult = Task.Run(() => iterator.ReadNextAsync()).GetAwaiter().GetResult();

        //    //cust = queryResult;//.Count();
        //    foreach (var item in queryResult.Resource)
        //    {
        //        // Debug.WriteLine("Name: " + item.lastName + ", " + item.firstName);
        //    }


        //    //Get records with 'Smith' as lastname
        //    //sqlString = "SELECT * FROM c where c.lastName like 'S%'";
        //    //sqlString = "SELECT * FROM c where c.firstName like 'T%'";
        //    //query = new QueryDefinition(sqlString);
        //    //iterator = table.GetItemQueryIterator<Customer>(sqlString);

        //    //queryResult = Task.Run(() => iterator.ReadNextAsync()).GetAwaiter().GetResult();

        //    //foreach (var item in queryResult.Resource)
        //    //{
        //    //    //Debug.WriteLine("Name: " + item.lastName + ", " + item.firstName);
        //    //  //  Debug.WriteLine("FirstName: " + item.firstName);
        //    //}


        //};

       
        

            using (var client = new CosmosClient("AccountEndpoint=https://mindcept-cosmo.documents.azure.com:443/;AccountKey=QQ8vUzwQtI4H3DQG6OYvuVUDaswQuNcoZBgtP9qnzjnR97UVHg0AIV5eKELIPFnWszn9Zuqr7XiuffQep6rjHg==;"))
                {
                    var DB = client.GetDatabase("ChampionProductCosmoDB");
                    var table = client.GetContainer(DB.Id, "Customer2");
                


                var sqlString = "";// "Select * from c where c.customerId = " + Id;
                //http://localhost:7071/api/GetDBCustRecord?name=Smith
                if (!string.IsNullOrEmpty(name))
                    {
                    //sqlString = "Select * from c where c.lastName = " + name;
                    //this just gives me all 16 records the I filter out at the end.  Its dumb but I could not change the partitionkeydefinition so just exparimenting
                    sqlString = "Select * from c";

                }
                else //http://localhost:7071/api/GetDBCustRecord?Id=1
                {
                        sqlString = "Select * from c where c.customerId = " + Id;

                    }
                    
                    var query = new QueryDefinition(sqlString);
                    FeedIterator<Customer> iterator = table.GetItemQueryIterator<Customer>(sqlString);

                    Microsoft.Azure.Cosmos.FeedResponse<Customer> queryResult = Task.Run(() => iterator.ReadNextAsync()).GetAwaiter().GetResult();
                //Just playing around since I cound not access and change partitionkeydefinition 
                // customer = queryResult.FirstOrDefault();
                if (queryResult.Count > 1)
                {
                    List<Customer> custlist = new List<Customer>();
                    custlist = queryResult.ToList();
                    responseMessage = JsonConvert.SerializeObject(custlist);

                }
                else { 
                customer = (Customer)queryResult.Where(c => c.lastName == name).FirstOrDefault();// .FirstOrDefault();

                responseMessage = JsonConvert.SerializeObject(customer);
                }
            };


            return new OkObjectResult(responseMessage);
        }
    }
}
